# Getting Started
The eDisplay requires a web server to run properly. We use the apache web server. The installation and usage instructions vary by operating system.

## Instructions For Mac
To install apache for Mac, follow the instructions [here](https://medium.com/@JohnFoderaro/how-to-set-up-apache-in-macos-sierra-10-12-bca5a5dfffba). At the instructions "2.2 Create Two Project Directories", you should also clone the eDisplay project into Sites. 
## Instructions For Windows
To install for Windows, you should have a Linux Subsystem for Windows (WSL). The Ubuntu WSL is a popular choice if you aren’t using a different WSL already. Instruction for installing a WSL are [here](https://docs.microsoft.com/en-us/windows/wsl/install-win10). The following instructions are for the Ubuntu subsystem. For a different subsystem, reference the Unix instructions below. 

Once you have the Ubuntu WSL open, run

`sudo apt-get install apache2`

If you get a pop-up from a firewall, allow access and then run the command again. Once apache is installed, run

`sudo service apache2 start` 

Then check if apache is working by typing “localhost" into a browser window.

Once apache is running, clone the eDisplay project into var/www. Then the site will be viewable at localhost/eDisplay.
## Instructions For Unix
To install apache for Unix and Unix-like systems, follow the instructions [here](https://httpd.apache.org/docs/trunk/install.html). After installing Apache, clone the eDisplay into the [DocumentRoot](https://httpd.apache.org/docs/current/mod/core.html#documentroot) folder and then you should be able to make and view local changes. For Unix systems, common locations for the Document Root are [here](https://cwiki.apache.org/confluence/display/HTTPD/DistrosDefaultLayout).

## Troubleshooting
If the site is not displaying correctly (i.e. the graph is not showing and there is an error in the web console), restart apache and then reload the site.


